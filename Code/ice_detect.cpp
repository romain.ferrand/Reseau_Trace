/**
 * @file ice_detect.cpp
 * @brief Implementation of all the Iceberg detection system
 */

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include "controller.hpp"
#include "router.hpp"

using namespace std;

/**
 * @brief The main function of the iceberg detection system.
 */
int main(int argc, char *argv[])
{
    if(argc == 4)
    {
        string file = argv[1];
        size_t nb_router = atoi(argv[2]);
        double threshold_ice = atof(argv[3]);
        size_t learning_cycle = 100;
        ifstream data_input(file, ios::in);
        if(data_input.is_open())
        {
            cout<<"Begin: Iceberg detection..."<<endl;
            vector<Router*> routers;

            //Build the controller
            Controller controller(threshold_ice);

            //Build the routers
            for(size_t i = 0; i< nb_router; ++i) {
                Router* router =  new Router(controller,
                                             threshold_ice, nb_router);
                routers.push_back(router);
                controller.add_router(router);
            }
            for(size_t i = 0; i < learning_cycle; ++i)
            {
                for (auto& router : routers)
                {
                    router->receive_data(data_input);
                }
            }
            
            //Activate detection for each router
            for(auto& router : routers) {
                router->activate();
            }
            while(!data_input.eof())
            {
                for(auto& router : routers)
                {
                    router->receive_data(data_input);
                    router->update();
                    controller.update();
                }
            }
        }
        cout<<"End: Iceberg detection."<<endl;
    }
    else
    {
        cout<<"Usage: ice_detect [file_name] [nb_router] [threshold]\n"<<endl;
    }
    return EXIT_SUCCESS;
}

